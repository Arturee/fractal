*This project is public only for presentation purposes. You have no permission to use, modify, or share the software.*

### Technologies ###

C#, Windows Forms

### Introduction ###

This is a fractal painter and traveller. Mandelbrot set and paramterized Julia set are available. A fractal can be
zoomed in/out, panned, color layers can be chosen arbitrarity, the level of rendering detail can be increased and
the result can be save as an image of any resolution.

![demo](readme/fractal.gif)

### Deployment ###

Works out of the box on any Windows machine (where .NET is installed).