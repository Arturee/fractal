﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //poskytuje Color mimojine

namespace FractalWorld
{
    /// <summary>
    /// Seznam naimplementovanych schemat barev. Vyuziva se jako indexer v poli pro prechazeni mezi nimi.
    /// </summary>
    public enum BarevnaSchemata : byte { Duha, Vlastni };

    /// <summary>
    /// Poskytuje schema barev pro fraktalovy obrazek.
    /// </summary>
    public class BarevneSchema
    {
        protected int rychlost = 100;
        /// <summary>
        /// Pocet brevnych stupnu (ruznych barev v jednom spektru, nez se zacnou opakovat)
        /// </summary>
        /// <value>
        /// Minimum je 10 (cokoliv mensiho bude nahrazeno 10)
        /// </value>
        public int Rychlost
        {
            get { return rychlost; }
            set
            {
                if (value < 10)
                    rychlost = 10;
                else
                    rychlost = value;
            }
        }
        private int posun = 0;
        /// <summary>
        /// Kazde spektrum zacina svou prvni barvou na nule, posun je tedy od toho aby spektrum zacinalo nekde jinde (vyse)
        /// a timpadem muze na nule byt jina barva a spektrum se posune.
        /// </summary>
        /// <value>Vzdy vraci i nastavuje hodnotu mezi 0..rychlost-1 inkluzivne (vsechny hodnoty jsou na ne prevoditelne)</value>
        public int Posun
        {
            get
            {
                posun = posun % rychlost;    
                return posun;
            }
            set
            {
                posun = value % rychlost;
                if (posun < 0) //zaporny posun se prevede na odpovidajici kaldny
                    posun += rychlost;
            } //vetsi posun bude mit stejny efekt po zmoduleni
        }
        /// <summary>
        /// Umoznuje prechod mezi barevnymi formaty HSL a RGB
        /// </summary>
        /// <remarks>Vsechny vstupni hodnoty se musi pohybovat v intervalu [0.0, 1.0)</remarks>
        /// <param name="h">hue</param>
        /// <param name="sl">saturation</param>
        /// <param name="l">lightness</param>
        /// <returns>barva ve formatu RGB</returns>
        public static Color HslToRgb(double h, double sl, double l)
        {
            //!!(nejsem si jisty o licenci)http://www.geekymonkey.com/Programming/CSharp/RGB2HSL_HSL2RGB.htm
            //TODO: predelat dle tohoto clanku (free to use) http://richnewman.wordpress.com/about/code-listings-and-diagrams/hslcolor-class/

            //vsechny hodnoty (h,s,l) se pohybuji mezi 0.0 ... 1.0
            double v;
            double r, g, b;

            r = l;   //zacneme se sedou
            g = l;
            b = l;

            v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);

            if (v > 0)
            {
                double m;
                double sv;
                int sextant;
                double fract, vsf, mid1, mid2;

                m = l + l - v;
                sv = (v - m) / v;
                h *= 6.0;
                sextant = (int)h;
                fract = h - sextant;
                vsf = v * sv * fract;
                mid1 = m + vsf;
                mid2 = v - vsf;

                switch (sextant)
                {
                    case 0:
                        r = v;
                        g = mid1;
                        b = m;
                        break;
                    case 1:
                        r = mid2;
                        g = v;
                        b = m;
                        break;
                    case 2:
                        r = m;
                        g = v;
                        b = mid1;
                        break;
                    case 3:
                        r = m;
                        g = mid2;
                        b = v;
                        break;
                    case 4:
                        r = mid1;
                        g = m;
                        b = v;
                        break;
                    case 5:
                        r = v;
                        g = m;
                        b = mid2;
                        break;
                }
            }
            return Color.FromArgb((int)(r * 255.0f), (int)(g * 255.0f), (int)(b * 255.0f));
            //255.0f urcuje typ float konstanty pro compiler
        }
        /// <summary>
        /// Urci barvu dle aktualniho schematu a jeho aktualnich vlastnosti na zaklade integer.
        /// </summary>
        /// <param name="i">Nezaporna hodnota</param>
        /// <returns>Barva dana aktualnim schematem, ktera nalezi tomuto cislu.</returns>
        public virtual Color UrciBarvu(int i)
        {
            return Color.White;   
        }
    }
    /// <summary>
    /// Klasicke barvy duhy.
    /// </summary>
    class Duha : BarevneSchema
    {
        /// <summary>
        /// Zmeni cislo na barvu duhy.
        /// </summary>
        /// <param name="i"></param>
        /// <returns>barva duhy prislusna cislu</returns>
        public override Color UrciBarvu(int i)
        {
            return BarevneSchema.HslToRgb(((i + Posun) % Rychlost) / (double)Rychlost, 1.0, 0.5); //hue v intervalu [0,1)
        }
    }

    class VlastniSchema : BarevneSchema
    { 
        private Color[] barvy = new Color[]{ Color.Red, Color.Yellow, Color.Green, Color.Blue, Color.Purple };
        /// <summary>
        /// Pole barev vybranych uzivatelem.
        /// </summary>
        /// <value>vzdy neprazdne (prirazeni null nebo prazdneho pole nebude mit zadny efekt)</value>
        public Color[] Barvy
        {
            get { return barvy; }
            set
            {
                if ((value != null) & value.Length > 0)
                     barvy = value;
            }
        }
        /// <summary>
        /// Vraci nejblizsi mensi rychlost delitelnou aktualnim poctem barev, aby
        /// se dobre pocital pocet mezistupnu mezi kazdymi dvema barvami.
        /// Je-li rychlost mensi nez pocet barev, nastavi ji na pocet barev.
        /// </summary>
        public new int Rychlost
        {
            get
            {
                if (rychlost < barvy.Length)
                {
                    rychlost = barvy.Length;
                    return rychlost;
                }
                else
                {
                    return rychlost - (rychlost % barvy.Length);
                }
            }
        }

        /// <summary>
        /// Urci barvu dle uzivatelem vybranych barev, mezi vybranymi barvami dela plynule prechody.
        /// </summary>
        /// <remarks>Mezi barvami se jednoduse linearne prechazi v kazde slozce zvlast.</remarks>
        /// <param name="i"></param>
        /// <returns>barva prislusna tomuto cislu dle spektra vybraneho uzivatelem</returns>
        public override Color UrciBarvu(int i)
        {
 	        return urciBarvu(i, barvy);
        }
        /// <summary>
        /// Vnitrni funkce pro <c>public override Color UrciBarvu(int i)</c>
        /// </summary>
        /// <param name="i"></param>
        /// <param name="barvy">pole uzivatelem zadanych barev</param>
        /// <returns></returns>
        private Color urciBarvu(int i, Color[] barvy)
        {
            try
            {
                int barevnychMezistupnu = Rychlost / barvy.Length; //vzdy alespon 1, vzdy deleno bezezbytku
                int uroven = ((i + Posun) % Rychlost) + 1; //1..Rychlost
                int index = (uroven - 1) / barevnychMezistupnu; //0..barvy.Length-1
                int offset = (uroven - 1) % barevnychMezistupnu; //0..barevnychMezistupnu-1

                int dalsi_index;
                if (index == barvy.Length - 1)
                {
                    dalsi_index = 0;
                }
                else
                {
                    dalsi_index = index + 1;
                }
                Color a = barvy[index]; Color b = barvy[dalsi_index];
                double cast = (double)offset / barevnychMezistupnu; // [0..1)
                //linearni prechod mezi barvami
                int red = (int)(a.R + (b.R - a.R) * cast);
                int green = (int)(a.G + (b.G - a.G) * cast);
                int blue = (int)(a.B + (b.B - a.B) * cast);

                return Color.FromArgb(red, green, blue);
            }
            catch
            {
                //!! zpracovavani vyjimek je VELMI pomale - ale zadna by nemela nastat
                return Color.White;
            }
        }
    }
}
