﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //poskytuje Bitmapu
using System.Windows.Forms;

namespace FractalWorld
{
    /// <summary>
    /// Nahlizi na Bitmapu jako na komplexni rovinu a dava pristup k operacim s komplexni rovinou vuci teto Bitmape.
    /// </summary>
    public class VyrezKomplexniRoviny
    {
        /// <summary>
        /// tovarni nastaveni - jak bude viditelny vyrez komplexni roviny siroky
        /// </summary>
        private const double PUVODNI_REALNE_ROZPETI = 3.0;
        /// <summary>
        /// Bitmapa nad niz je budovan vyrez komplexni roviny
        /// </summary>
        public Bitmap Bmp;
        /// <summary>
        /// Kanvas, ktery drzi bitmapu - je potrebny kdyz dochazi ke skalovani aby spravne fungoval zoom a posunuti.
        /// </summary>
        private PictureBox kanvas;
        private double reRozpeti = PUVODNI_REALNE_ROZPETI;
        /// <summary>
        /// Sirka viditelne casti komplexni roviny.
        /// </summary>
        public double ReRozpeti
        {
            get { return reRozpeti; }
        }
        /// <summary>
        /// Vyska viditelne casti realne roviny
        /// </summary>
        public double ImRozpeti
        {
            get
            {
                return Bmp.Height * SirkaPixelu;
            }
        }
        private Komplex stred = new Komplex(-1.0, 0.0);
        /// <summary>
        /// Komplexni cislo ktere se nachazi ve stredu bitmapy
        /// </summary>
        public Komplex Stred
        {
            get { return stred; }
        }
        /// <summary>
        /// Delka kterou v komplexni rovine zabere 1 pixel (co do sirky nebo do vysky) vuci bitmape
        /// </summary>
        /// <remarks>hraje roli pri vykreslovani bitmapy (to co se deje schovane v pameti)</remarks>
        public double SirkaPixelu
        {
            get
            {
                return ReRozpeti / Bmp.Width;
            }
        }
        /// <summary>
        /// Delka kterou v komplexni rovine zabere 1 pixel ale vuci picture boxu
        /// </summary>
        /// <remarks>Hraje roli pri zoomu, posunuti a zmene velikosti okna</remarks>
        public double SirkaPixelu_vuciPicturBoxu
        {
            get
            {
                return ReRozpeti / kanvas.ClientSize.Width;
            }
        }
        /// <summary>
        /// Zvetseni vuci tovarnimu nastaveni (pri zapnuti je zvetseni = 1)
        /// </summary>
        /// <remarks>zvetseni je typu double aby mohlo dosahovat extremnich hodnot</remarks>
        public double Zvetseni
        {
            get { return PUVODNI_REALNE_ROZPETI / ReRozpeti; }
        }

        /// <summary>
        /// Nacte kanvas a jemu prislusejici bitmapu
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="kanvas"></param>
        public VyrezKomplexniRoviny(Bitmap bmp, PictureBox kanvas)
        {
            Bmp = bmp;
            this.kanvas = kanvas;
        }
        /// <summary>
        /// Priradi pixelu odpovidajici komplexni cislo vuci aktualnimu vyrezu komplexni roviny.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Komplex PriradKomplexniCislo(int x, int y)
        {
            //ma-li bitmapa sudou vysku nebo sirku, na privni(nulou pojmenovany) pixel se zapomene a jako stred(komplexni roviny) se povazuje stred zbyleho licheho poctu
            //ma-li bitmapa lichou sirku, pak prostredni pixel ma souradnici x = Bmp.Width / 2;
            int prostredniPixel_x = Bmp.Width / 2;
            int prostredniPixel_y = Bmp.Height / 2;

            return new Komplex(stred.Re + (x - prostredniPixel_x) * SirkaPixelu, stred.Im - (y - prostredniPixel_y) * SirkaPixelu);
        }
        /// <summary>
        /// Nakonfiguruje komplexni rovinu dle rucniho nastaveni
        /// </summary>
        /// <remarks>zaporne realne rozpeti bude prevedeno na kladne a rozpeti = 0.0 bude nahrazeno rozpetim 1.0</remarks>
        /// <param name="stred">Novy stred</param>
        /// <param name="realneRozpeti">Nove realne rozpeti - nezaporne cislo</param>
        public void ZmenSouradnice(Komplex stred, double realneRozpeti)
        {
            this.stred = stred;
            if (realneRozpeti != 0.0)
                reRozpeti = Math.Abs(realneRozpeti);
            else
                reRozpeti = 1.0;
        }
        /// <summary>
        /// Provede zoom - zmeni stred komplexni roviny a priblizi/oddali tim, ze zmensi/zvetsi realne rozpeti (a automaticky i imaginarni).
        /// </summary>
        /// <param name="kliknuti_x">x souradnice kliknuti pocitano odzhora vu picture boxu</param>
        /// <param name="kliknuti_y">y souradnice kliknuti pocitano odzhora vu picture boxu</param>
        /// <param name="priblizit">true = priblizit, false = oddalit</param>
        /// <param name="kolikrat">hodnota > 2</param>
        public void Zoomuj(int kliknuti_x, int kliknuti_y, bool priblizit, int kolikrat)
        {
            double oKolikDoleva = (kanvas.ClientSize.Width / 2 - kliknuti_x) * SirkaPixelu_vuciPicturBoxu;
            double oKolikNahoru = (kanvas.ClientSize.Height / 2 - kliknuti_y) * SirkaPixelu_vuciPicturBoxu;

            //novy stred
            stred.Re -= oKolikDoleva;
            stred.Im += oKolikNahoru;
            if (priblizit) //zvetseni
            {
                reRozpeti /= kolikrat;
            }
            else //zmenseni
            {
                reRozpeti *= kolikrat;
            }
        }
        /// <summary>
        /// Posune vyrez komplexni roviny tim, ze posune jeji stred.
        /// </summary>
        /// <param name="xNew">souradnice x konce tahani vuci picture boxu</param>
        /// <param name="xOld">souradnice x pocatku tahani vuci picture boxu</param>
        /// <param name="yNew">souradnice y konce tahani vuci picture boxu</param>
        /// <param name="yOld">souradnice y pocatku tahani vuci picture boxu</param>
        public void Posun(int xNew, int xOld, int yNew, int yOld)
        {
            //zmeni stred po posunuti (drag)
            double oKolikDoleva = (xNew - xOld) * SirkaPixelu_vuciPicturBoxu;
            double oKolikNahoru = (yNew - yOld) * SirkaPixelu_vuciPicturBoxu;

            stred.Re -= oKolikDoleva;
            stred.Im += oKolikNahoru;
        }
    }
}
