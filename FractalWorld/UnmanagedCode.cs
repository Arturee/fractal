﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices; //poskytjuje DllImport

namespace FractalWorld
{
    /// <summary>
    /// Obsahuje metody unamanaged kodu - volane z externich .dll (napr. Win32 API)
    /// </summary>
    /// <remarks>Externi .dll jsou zkompilovane vuci pocitaci, na kterem jsou ulozene - nejedna se tedy o CIL kod.</remarks>
    static class UnmanagedCode
    {
        /// <summary>
        ///  Zabranuje aby se okno stalo Unresponsive.
        /// </summary>
        /// <remarks>Volani Win32 API.</remarks>
        [DllImport("user32.dll")]
        public static extern void DisableProcessWindowsGhosting();
    }
}
