﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FractalWorld
{
    /// <summary>
    /// Kalsicka komplexni cisla jako struct a implementovana pomoci double
    /// </summary>
    /// <remarks>Je s nimi rychlejsi prace nez s objektem a alokuji se na zasobniku. Pozor na deleni komplexni nulou - dojde k neosetrene
    /// vyjimce kvuli deleni nulou u typu double</remarks>
    public struct Komplex
    {
        //pozor na deleni komplexni nulou
        //0/0 isNaN, ne-nula/0 isInfinity
        /// <summary>
        /// Realna cast komplexniho cisla
        /// </summary>
        public double Re;
        /// <summary>
        /// Imaginarni cast komplexniho cisla
        /// </summary>
        public double Im;
        /// <summary>
        /// Vytvori nove Komplexni cislo dle zadani alokovane na zasobniku.
        /// </summary>
        /// <param name="re">realna cast komplexniho cisla</param>
        /// <param name="im">imaginarni cast komplexniho cisla</param>
        public Komplex(double re, double im)
        {
            Re = re;
            Im = im;
        }
        /// <summary>
        /// Vzdalenost komplexniho cisla od komplexniho pocatku.
        /// </summary>
        public double Norma
        {
            get
            {
                return Math.Sqrt(Re * Re + Im * Im);
            }
        }
        public static Komplex operator +(Komplex k, Komplex l)
        {
            return new Komplex(k.Re + l.Re, k.Im + l.Im);
        }
        public static Komplex operator -(Komplex k, Komplex l)
        {
            return new Komplex(k.Re - l.Re, k.Im - l.Im);
        }
        public static Komplex operator *(Komplex k, Komplex l)
        {
            return new Komplex(k.Re * l.Re - k.Im * l.Im, k.Im * l.Re + k.Re * l.Im);
        }
        public static Komplex operator /(Komplex k, Komplex l)
        {
            return new Komplex((k.Re * l.Re + k.Im * l.Im) / (l.Re * l.Re + l.Im * l.Im), (k.Im * l.Re - k.Re * l.Im) / (l.Re * l.Re + l.Im * l.Im));
        }
        public override string ToString()
        {
            return String.Format("{0} + {1}i", Re, Im);
        }
    }
}
