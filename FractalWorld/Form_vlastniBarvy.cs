﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{
    /// <summary>
    /// Dava moznost vyberu lastnich barev rucne nebo nahodne. Pocet barev je mezi 1 a 33.
    /// </summary>
    /// <remarks>Velikost okna a minimalni velikost okna se meni spravne vzhledem k poctu nagenerovanych kachlicek</remarks>
    public partial class Form_vlastniBarvy : Form
    {
        private const int MAX_POCET_KACHLICEK = 33;
        private const int SIRKA_KACHLICKY = 25;
        private int pocetKachlicek = 0;
        
        private const int ZMENA_VYSKY = 40;

        /// <summary>
        /// Bude pouzito pro randomizaci barev.
        /// </summary>
        private Random r = new Random();
        /// <summary>
        /// Nacte barevne kachlicky dle puvodnich vlastnich barev. Je-li vstup null, vytvori 1 bilou kachlicku.
        /// </summary>
        /// <param name="barvy">Pole barev z barevneho schema vlastni barvy.</param>
        public Form_vlastniBarvy(Color[] barvy)
        {
            InitializeComponent();
            try
            {
                for (int i = 0; i < barvy.Length; i++)
                {
                    pridejKachlicku().BackColor = barvy[i];
                }
            }
            catch
            {
                pridejKachlicku().BackColor = Color.White;
            }
        }
        /// <summary>
        /// Prida novou kachlicku do flowLayoutPanelu, pokud uz jich tam neni moc.
        /// </summary>
        /// <remarks>Je-li potreba, vhodne zmeni velikost a minimalni velikost okna vuci kachlickam.</remarks>
        /// <returns>odkaz na kachlicku - aby sla jednoduse obarvit - nebo null, je-li kachlicek moc.</returns>
        private Button pridejKachlicku()
        {
            if (pocetKachlicek >= MAX_POCET_KACHLICEK)
                return null;

            Button kachlicka = new Button();
            pocetKachlicek++;
            kachlicka.Name = String.Format("kachlicka{0}", pocetKachlicek);
            kachlicka.TabIndex = pocetKachlicek-1;
            kachlicka.Size = new Size(SIRKA_KACHLICKY, SIRKA_KACHLICKY);
            kachlicka.UseVisualStyleBackColor = true;
            flowLayoutPanel1.Controls.Add(kachlicka);
            kachlicka.Click += new System.EventHandler(kachlicka_Click);

            if (pocetKachlicek % 10 == 1)
            {
                nastavVyskuFlowPanelu(ZMENA_VYSKY);
            }
            return kachlicka;
        }
        /// <summary>
        /// Nabidne vyber barvy pro kachlicku.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void kachlicka_Click(object sender, EventArgs e)
        {
            using (ColorDialog cd = new ColorDialog())
            {
                if (cd.ShowDialog() == DialogResult.OK)
                {
                    ((Button)sender).BackColor = cd.Color;
                }
            }
        }
        /// <summary>
        /// Prida novou kachlicku s tabIndexem o 1 vetsim (pocinaje od 0) a obarvenou na bilo.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_add_Click(object sender, EventArgs e)
        {
            pridejKachlicku().BackColor = Color.White;
        }
        /// <summary>
        /// Odebere posledni kachlicku, je-li kachlicek vice nez 1.
        /// </summary>
        /// <remarks>Je-li potreba, vhodne zmeni velikost a minimalni velikost okna vuci kachlickam.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_remove_Click(object sender, EventArgs e)
        {
            if (pocetKachlicek <= 1)
                return;
            foreach (Control c in flowLayoutPanel1.Controls)
                if (c.TabIndex == (pocetKachlicek-1)) //posledni kachlicka
                {
                    c.Dispose();
                    pocetKachlicek--;
                }
            if (pocetKachlicek % 10 == 0) //zmena velikosti okna
            {
                nastavVyskuFlowPanelu(-ZMENA_VYSKY);
                tableLayoutPanel_hlavni.Height -= ZMENA_VYSKY;
                this.Height -= ZMENA_VYSKY;
            }
        }
        /// <summary>
        /// Precte barvy z barevnych kachlicek(pole je vzdy alespon jednoprvkove).
        /// </summary>
        /// <returns>Pole barev z kachlicek</returns>
        public Color[] PrectiBarvy()
        {
            Color[] barvy = new Color[pocetKachlicek];
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                barvy[c.TabIndex] = ((Button)c).BackColor;
            }
            return barvy;
        }

        /// <summary>
        /// Vsechny barvy jsou stejne pravdepodobne vuci RGB.
        /// </summary>
        /// <returns>Nahodne zvolena barva</returns>
        private Color generujNahodnouBarvu()
        {
            return Color.FromArgb(r.Next(256), r.Next(256), r.Next(256));
        }
        /// <summary>
        /// Zvoli nahodne barvy pro vsechny kachlicky, ktere jsou na okne.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_randomize_Click(object sender, EventArgs e)
        {
            foreach (Control c in flowLayoutPanel1.Controls)
            {
                ((Button)c).BackColor = generujNahodnouBarvu();
            }
        }
        /// <summary>
        /// Zvetsi nebo zmensi vysku flow panelu.
        /// </summary>
        /// <param name="zmenaVysky"></param>
        private void nastavVyskuFlowPanelu(int zmenaVysky)
        {
            flowLayoutPanel1.Height += zmenaVysky;
            nastavMinima(zmenaVysky);
        }
        /// <summary>
        /// Nastavi minimum size pro tento Form, aby nedoslo ke schovani kachlicek pri zmene velikosti okna.
        /// </summary>
        /// <param name="zmenaVysky"></param>
        private void nastavMinima(int zmenaVysky)
        {
            this.MinimumSize = new Size(this.MinimumSize.Width, this.MinimumSize.Height + zmenaVysky);
        }
    }
}
