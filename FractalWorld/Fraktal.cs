﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FractalWorld
{
    /// <summary>
    /// Seznam naimplementovanych fraktalu, vyuziva se pro prechazeni mezi nimi.
    /// </summary>
    public enum Fraktaly : byte { Mandelbrot, JuiliaJednoduchaKvadraticka };

    /// <summary>
    /// Od teto tridy budou dedit frataly, ktere jsou zalozene na tomto principu: vezme se komplexni cislo, toto cislo generuje komplexni posloupnost, pokud posloupnost
    /// konverguje vuci norme,tak dane cislo patri do fraktalni mnoziny, jinak nepatri.
    /// </summary>
    public class Fraktal
    {
        /// <summary>
        /// Na zaklade komplexniho cisla zacne generovat posloupnost a overi (primitivne) zda konverguje.
        /// </summary>
        /// <param name="k"></param>
        /// <param name="prahKonvergence">Pocet clenu posloupnosti pred nimz nezacla-li posloupnost divergovat, tak uz se povazuje za konvergentni.</param>
        /// <param name="konverguje">True pokud posloupnost konverguje, false jinak</param>
        /// <returns>Cislo - clen kdy posloupnost zacala divergovat. Pokud konverguje tak toto cislo nema vyznam (a bude rovno prahuKonvergence).</returns>
        public virtual int TestujKonvergenci(Komplex k, int prahKonvergence, out bool konverguje)
        {
            // falesne navratove hodnoty pro kompilator
            konverguje = false;
            return 1;
        }
    }

    /// <summary>
    /// Charakteristika mandelbrotovy mnoziny.
    /// </summary>
    /// <remarks>Rekurzni vzorecek Z0, Zn=Zn^2 + Z0</remarks>
    public class Mandelbrot : Fraktal
    {
        public override int TestujKonvergenci(Komplex k, int prahKonvergence, out bool konverguje)
        {
            Komplex Zn = k; //prvni clen posloupnosti

            for (int i = 1; i < prahKonvergence; i++)
            {
                Zn = (Zn * Zn) + k; //dalsi clen dany rekurzi
                if (Zn.Norma >= 2)
                {
                    //pak Zn v dalsich iteracich musi divergovat
                    konverguje = false;
                    return i; //vraci 1..prah.TKonvergence-1
                }
            }
            //i == prahKonvergence
            konverguje = true;
            return prahKonvergence;
        }
    }
    /// <summary>
    /// Charakteristika Julia mnoziny.
    /// </summary>
    /// <remarks>Rekurzni vzorecek Z0, Zn=Zn^2 + seminko</remarks>
    public class Julia_quadratic : Fraktal
    {
        private Komplex seminko = new Komplex(0.279, 0.0);
        /// <summary>
        /// Seminko, ktere meni vzhled fraktalu.
        /// </summary>
        /// <value>seminko</value>
        public Komplex Seminko
        {
            get { return seminko; }
            set { seminko = value; }
        }

        //doporucena seminka jsou:

        //0.279 + 0.0i
        //0.432 + 0.432i
        //necht G = 1.61803398875 (zlaty rez)
        //1-G
        //G-2 + (G-1)i
        //-0.8 + 0.156i

        public override int TestujKonvergenci(Komplex k, int prahKonvergence, out bool konverguje)
        {
            Komplex Zn = k; //prvni clen posloupnosti

            for (int i = 1; i < prahKonvergence; i++)
            {
                Zn = (Zn * Zn) + seminko; //zde je rozdil od Mandelbrota
                if (Zn.Norma >= 2)
                {
                    konverguje = false;
                    return i; //vraci 1..prahKonvergence-1
                }
            }
            //i == prahKonvergence
            konverguje = true;
            return prahKonvergence;
        }
    }
}
