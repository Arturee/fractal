﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{
    /// <summary>
    /// Umozni zmenu rozliseni.
    /// </summary>
    /// </remarks>
    /// Maximalni rozliseni v lib. smeru je int.Maxvalue, minimalni je 1.</remarks>
    public partial class Form_rozliseni : Form
    {

        /// <summary>
        /// Nastavi maximum jako int.MaxValue a nacte zadana rozliseni.
        /// </summary>
        /// <param name="rozliseni_x">puvodni rozliseni sirka</param>
        /// <param name="rozliseni_y">puvodni rozliseni vyska</param>
        public Form_rozliseni(int rozliseni_x, int rozliseni_y)
        {
            InitializeComponent();
            numericUpDown_sirka.Maximum = int.MaxValue;
            numericUpDown_vyska.Maximum = int.MaxValue;
            numericUpDown_sirka.Value = rozliseni_x;
            numericUpDown_vyska.Value = rozliseni_y;
        }
        /// <returns>Rozliseni nasirku</returns>
        public int PrectiSirku()
        {
            return (int)numericUpDown_sirka.Value;
        }
        /// <returns>Rozliseni navysku</returns>
        public int PrectiVysku()
        {
            return (int)numericUpDown_vyska.Value;
        }
    }
}
