﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{
    public partial class Form_navodNaPouziti : Form
    {
        public Form_navodNaPouziti()
        {
            InitializeComponent();
            textBox1.Text = @"
You can

    - Zoom in by double-clicking
    - Zoom out by right-clicking
    - Move sideways by holding, dragging and releasing

In Colors > Switch > Custom you can click a colorful tile to change its color.

Did you know that exploring the fractal in a small window is much faster?
Did you know that exploring white territory is very slow and also pointless?
Did you know that white territory may become colorful if you increase Detail?
You can change the color of white territory to other than white in Colors > Inner color.

Did you know that saving the image as .png yields best quality images?
";
        }
    }
}







