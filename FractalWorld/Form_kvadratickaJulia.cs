﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{
    /// <summary>
    /// Umoznuje menit seminko pro jednoduchou kvadratickou Juliu, jejiz generujici posloupnost
    /// je dana rekruznim vzoreckem Zn = Zn*Zn + seminko.
    /// </summary>
    public partial class Form_kvadratickaJulia : Form
    {
        private Komplex seminko;
        private Random r = new Random();
        private bool vstupJeValidni = true;

        /// <summary>
        /// Nahraje do okenka aktualni seminko.
        /// </summary>
        /// <param name="seminko">seminko pro jednoducho kvadratickou Juliu, ktere bylo v programu nastavene pred otevrenim tohoto okna</param>
        public Form_kvadratickaJulia( Komplex seminko )
        {
            InitializeComponent();
            this.seminko = seminko;
            inicializujPolicka();
            nastavNormu();
        }
        /// <summary>
        /// Vrati seminko prectene z textBoxu.
        /// </summary>
        /// <returns>
        /// Pokud realna nebo imaginarni cast nejde precist, vrati puvodni seminko (pouzite pro vytvoreni tohoto dialogu).
        /// </returns>
        public Komplex PrectiSeminko()
        {
            return seminko;
        }
        /// <summary>
        /// Nacte vybrane seminko z komboboxu do vhodnych texBoxu. Doporucena seminka davaji hezke fraktaly.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_recommended_SelectedIndexChanged(object sender, EventArgs e)
        {
            double zlatyRez = 1.61803398875; //priblizne
            switch (comboBox_recommended.SelectedIndex)
            {
                //pokud uzivatel nic nezvoli, selectedIndex bude -1
                case 0:
                    textBox_real.Text = "0.279";
                    textBox_imaginary.Text = "0.0";
                    break;
                case 1:
                    textBox_real.Text = textBox_imaginary.Text = "0.432";
                    break;
                case 2:
                    textBox_real.Text = (1 - zlatyRez).ToString();
                    textBox_imaginary.Text = "0.0";
                    break;
                case 3:
                    textBox_real.Text = (zlatyRez - 2).ToString();
                    textBox_imaginary.Text = (zlatyRez - 1).ToString();
                    break;
                case 4:
                    textBox_real.Text = "-0.8";
                    textBox_imaginary.Text = "0.156";
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Zvoli realnou a komplexni cast seminka nahodne, ale dostatecne male, aby bylo malo pravdepodobne, ze se misto fraktalu vykresli jednobarevna plocha
        /// </summary>
        /// <remarks>
        /// Kazdou cast zvoli mensi nez 2, jelikoz vyssi hodnoty urcite
        /// znamenaji, ze norma seminka bude vyssi nez 2 a pak generujici posloupnost (zadana vzoreckem Zn = Zn*Zn + seminko)
        /// bude divergovat ve vsech bodech - timpadem se misto fraktalu vykresli jednobarevna rovina.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_randomize_Click(object sender, EventArgs e)
        {
            double real = r.NextDouble() * 2;
            double imag = r.NextDouble() * 2;
            int sign = r.Next(2);
            if (sign == 1)
            {
                real = -real;
            }
            sign = r.Next(2);
            if (sign == 1)
            {
                imag = -imag;
            }
            textBox_real.Text = real.ToString();
            textBox_imaginary.Text = imag.ToString();
        }
        /// <summary>
        /// nacte hodnoty seminka, ktere bylo v programu pred spustenim othoto okna do textochyv policek
        /// </summary>
        private void inicializujPolicka()
        {
            inicializujPolicka(seminko);
        }
        /// <summary>
        /// Vnitrni funkce pro <c>private void inicializujPolicka()</c>
        /// </summary>
        /// <param name="seminko">seminko pro jednoduchou kvadratickou Juliu, ktere bude nactene do textovych policek
        /// tohoto okna</param>
        private void inicializujPolicka(Komplex seminko)
        {
            textBox_real.Text = seminko.Re.ToString();
            textBox_imaginary.Text = seminko.Im.ToString();
        }
        /// <summary>
        /// Zkontroluje zda je vstup validni, pokud ano, nastavi dialog result na OK a zavre se, pokud ne,
        /// nahlasi problem a vrati textova policka do puvodniho stavu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_ok_Click(object sender, EventArgs e)
        {
            if (vstupJeValidni)
                DialogResult = DialogResult.OK;
            else
            {
                MessageBox.Show("The values you chose are wrong. Please use the original format.", "Careful", MessageBoxButtons.OK, MessageBoxIcon.Error);
                inicializujPolicka();
            }
        }
        /// <summary>
        /// Nastavi realnou cast seminka, pokud byl spatny vstup, nahlasi problem a nastavi realnou cast seminka v textboxu na posledni platnou.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_real_TextChanged(object sender, EventArgs e)
        {
            double realna;
            if (Double.TryParse(textBox_real.Text, out realna))
            {
                seminko.Re = realna;
                nastavNormu();
            }
            else
            {
                MessageBox.Show("The value you chose is wrong. Please use the original format.", "Careful", MessageBoxButtons.OK, MessageBoxIcon.Error);
                inicializujPolicka();
            }
        }
        /// <summary>
        /// Nastavi imaginarni cast seminka, pokud byl spatny vstup, nahlasi problem a nastavi imaginarni cast seminka v textboxu na posledni platnou.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_imaginary_TextChanged(object sender, EventArgs e)
        {
            double imaginarni;
            if (Double.TryParse(textBox_imaginary.Text, out imaginarni))
            {
                seminko.Im = imaginarni;
                nastavNormu();
            }
            else
            {
                MessageBox.Show("The value you chose is wrong. Please use the original format.", "Careful", MessageBoxButtons.OK, MessageBoxIcon.Error);
                inicializujPolicka();
            }
        }
        /// <summary>
        /// napise na okno jaka je vzdalenost od pocatku aktualniho seminka, ktere je v textovych policcich
        /// </summary>
        private void nastavNormu()
        {
            label_vzdalenostOdPocatku.Text = String.Format("The distance form the origin is: {0:E10}", seminko.Norma.ToString());
        }
    }
}
