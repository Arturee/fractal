﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{

    /// <summary>
    /// Umoznuje nastaveni komplexni roviny, diky nastaveni stredu a realneho rozpeti, coz staci na urceni
    /// vyseku (neroztazene) komplexni roviny.
    /// Navic ukazuje zvetseni.
    /// </summary>
    public partial class Form_souradnice : Form
    {
        private Komplex puvodniStred;
        /// <summary>
        /// kaldne cislo.
        /// </summary>
        private double puvodniRealneRozpeti;
        private Komplex novyStred;
        /// <summary>
        /// kladne cislo.
        /// </summary>
        private double noveRealneRozpeti;

        /// <summary>
        /// Nacte vlastnosti okenka, pritom hlida korektnost.
        /// </summary>
        /// <remarks>Je-li realne rozpeti zaporne, bude nactena absolutni hodnota, je-li rovno 0.0, bude umele nahrazeno cislem 1.0 (k tomu by ale v zadnem pripade nemelo dojit).</remarks>
        /// <param name="stred"></param>
        /// <param name="realneRozpeti">Kaldne cislo.</param>
        /// <param name="zvetseni">Kaldne cislo.</param>
        public Form_souradnice(Komplex stred, double realneRozpeti, double zvetseni)
        {
            InitializeComponent();

            puvodniStred = stred;
            if (realneRozpeti == 0.0) //tato kontrola je zbytecna ... pouze vhodna kdyby vyvojar pouzil tento form spatne
                puvodniRealneRozpeti = 1.0;
            else
                puvodniRealneRozpeti = Math.Abs(realneRozpeti);

            this.puvodniRealneRozpeti = realneRozpeti;
            this.puvodniStred = stred;
            inicializujPolicka();
            label_zvetseni.Text += String.Format(" {0:E3}", zvetseni);
        }
        /// <returns>Novy zadany stred.</returns>
        public Komplex PrectiStred()
        {
            return novyStred;
        }
        /// <returns>Nove zadane realne rozpeti.</returns>
        public double PrectiRealneRozpeti()
        {
            return noveRealneRozpeti;
        }
        /// <summary>
        /// zkontorluje zda je vstup validni a take naparsovany vstup ulozi jako vlastnosti okenka noveRealneRozpeti a novyStred.
        /// </summary>
        /// <returns>true pokud je vstup validni, false jinak</returns>
        private bool jeVstupValidni()
        {
            double rozpeti, realna, imaginarni;
            if (Double.TryParse(textBox_realneRozpeti.Text, out rozpeti) && (rozpeti > 0.0) && Double.TryParse(textBox_realna.Text, out realna) && Double.TryParse(textBox_imaginarni.Text, out imaginarni))
            {
                noveRealneRozpeti = rozpeti;
                novyStred = new Komplex(realna, imaginarni);
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Zkontroluje zda jsou udaje validni, pokud ano - nastavi dailog result na OK a zavre se,
        /// jinak nahlasi problem a vrati textova policka do puvodniho stavu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_ok_Click(object sender, EventArgs e)
        {
            if (jeVstupValidni())
                DialogResult = DialogResult.OK;
            else
            { 
                //byl zadany nesmyslny nebo spatny vstup
                MessageBox.Show("The values you chose are wrong. Please use the original format and check that real width is positive.", "Careful", MessageBoxButtons.OK, MessageBoxIcon.Error);
                inicializujPolicka();
            }
        }
        /// <summary>
        /// nacte do textovych policek nastaveni, ktere bylo v programu pred otevrenim tohoto okna
        /// </summary>
        private void inicializujPolicka()
        {
            inicializujPolicka(puvodniStred, puvodniRealneRozpeti);
        }
        /// <summary>
        /// Vnitrni funkce pro <c>private void inicializujPolicka()</c>
        /// </summary>
        /// <param name="stred">stred komplexni roviny nastaveny v programu pred vyskocenim tohoto okna</param>
        /// <param name="realneRozpeti">sirka komplexni roviny nastavena v programu pred spustenim tohoto okna</param>
        private void inicializujPolicka(Komplex stred, double realneRozpeti)
        {
            textBox_realna.Text = String.Format("{0:E10}", stred.Re);
            textBox_imaginarni.Text = String.Format("{0:E10}", stred.Im);
            textBox_realneRozpeti.Text = String.Format("{0:E10}", realneRozpeti); 
        }
    }
}


