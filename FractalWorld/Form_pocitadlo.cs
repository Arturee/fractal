﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{
    /// <summary>
    /// Jednoduse modifikovatelne okenko na nastaveni 1 integer hodnoty.
    /// </summary>
    public partial class Form_pocitadlo : Form
    {
        /// <summary>
        /// Nastavi vlastnosti okenka dle zadani a prizpusobi jeho minimalni velikost sirce napisu nad pocitadlem.
        /// </summary>
        /// <param name="hodnota">Puvodni hodnota na pocitadlo</param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="titul">Titul okenka</param>
        /// <param name="napis">Nadpisek nad pocitadlem</param>
        public Form_pocitadlo(int hodnota, int min, int max, string titul, string napis)
        {
            InitializeComponent();
            this.Text = titul;
            label1.Text = napis;
            int sirka = Math.Max(290, label1.Width + 50);
            this.Width = sirka;
            this.MinimumSize = new Size(sirka, this.Height);
            numericUpDown1.Minimum = min;
            numericUpDown1.Maximum = max;
            numericUpDown1.Value = hodnota;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Hodnotu z pocitadla (neco mezi min a max inkluzivne).</returns>
        public int PrectiHodnotu()
        {
            return (int)numericUpDown1.Value;
        }
    }
}
