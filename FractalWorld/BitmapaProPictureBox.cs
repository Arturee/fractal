﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; //poskytuje pictureBox
using System.Drawing; //poskytuje Bitmapu

namespace FractalWorld
{
    /// <summary>
    /// Vytvari Bitmapu a prizpusobuje ji danemu pictureBoxu co do velikosti, dale dovoluje nastavit rozliseni rucne, potom
    /// zajisti aby se bitmapa roztahla tak aby pokryla cely pictureBox
    /// </summary>
    public class BitmapaProPictureBox
    {
        /// <summary>
        /// pictureBox vuci kteremu budeme ovladat bitampu
        /// </summary>
        public PictureBox Kanvas;
        /// <summary>
        /// bitmapa pro dany picturebox
        /// </summary>
        public Bitmap Bmp = new Bitmap(100, 100); //inicializace, aby nebyl problem s nullReferenci
        private int rozliseni_X, rozliseni_Y;
        /// <summary>
        /// sirka bitmapy v pixelech
        /// </summary>
        public int Rozliseni_X
        {
            get { return rozliseni_X;  }
        }
        /// <summary>
        /// vyska bitmapy v pixelech
        /// </summary>
        public int Rozliseni_Y
        {
            get { return rozliseni_Y;  }
        }
        /// <summary>
        /// rika zda bylo rozliseni nastaveno rucne, pak se pitmapa nebude konfigurovat dle pictureboxu ale naskaluje se do nej
        /// </summary>
        private bool rucneNastaveneRozliseni = false;
        /// <summary>
        /// Nahraje pictureBox a hned mu prizpusobi bitmapu
        /// </summary>
        /// <param name="kanvas">pictureBox vuci kteremu bude vytvorena a ovladana bitmapa</param>
        public BitmapaProPictureBox(PictureBox kanvas)
        {
            Kanvas = kanvas;
            kanvas.SizeMode = PictureBoxSizeMode.StretchImage;
            ObnovBitmapu();
        }
        /// <summary>
        /// Prizpusobi bitmapu pictureBoxu, nebylo-li rozliseni nastavene rucne. Jinak vytvori bitmapu dle nastaveneho rozliseni a nechaji naskalovat do pictureBoxu.
        /// </summary>
        /// <returns>Odkaz na aktualni bitmapu. (nidky null)</returns>
        public Bitmap ObnovBitmapu()
        {
            if (rucneNastaveneRozliseni)
            {
                obnovBitmapu(rozliseni_X, rozliseni_Y);
                rucneNastaveneRozliseni = false;
            }
            else
                obnovBitmapu();
            return Bmp;
        }
        /// <summary>
        /// Nakonfiguruje bitmapu dle picture boxu
        /// </summary>
        private void obnovBitmapu()
        {
            //zahodi starou a vytvori novou bitmapu, odpovidajici velikosti (odpovida velikosti ObrazBoxu)
            Bmp.Dispose();
            rozliseni_X = Kanvas.Width;
            rozliseni_Y = Kanvas.Height;
            Bmp = new Bitmap(rozliseni_X, rozliseni_Y);
            Kanvas.Image = Bmp;
        }
        /// <summary>
        /// Nakonfiguruje bitmapu dle rucne zvoleneho rozliseni.
        /// </summary>
        /// <param name="rozliseni_X">Pocet pixelu na sirku</param>
        /// <param name="rozliseni_Y">Pocet pixelu na vysku</param>
        private void obnovBitmapu(int rozliseni_X, int rozliseni_Y)
        {
            Bmp.Dispose();
            this.rozliseni_X = rozliseni_X;
            this.rozliseni_Y = rozliseni_Y;
            Bmp = new Bitmap(rozliseni_X, rozliseni_Y);
            Kanvas.Image = Bmp;
        }
        /// <summary>
        /// Rucne nastavi rozliseni bitmapy
        /// </summary>
        /// <remarks>A zapamatuje si ze se tak stalo.</remarks>
        /// <param name="rozliseni_X"></param>
        /// <param name="rozliseni_Y"></param>
        public void nastavRozliseni(int rozliseni_X, int rozliseni_Y)
        {
            this.rozliseni_X = rozliseni_X;
            this.rozliseni_Y = rozliseni_Y;
            rucneNastaveneRozliseni = true;            
        }
    }
}
