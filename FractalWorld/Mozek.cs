﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing; // poskytuje Colour
using System.Windows.Forms;
using System.Threading.Tasks;

namespace FractalWorld
{
    /// <summary>
    /// Ridici struktura. Ovlada bitmapu, umoznuje vykreslovat fraktaly, menit barevna schemata a pracovat s komplexni rovinou.
    /// </summary>
    public class Mozek
    {
        /// <summary>
        /// Barva, kterou dostanou body, ktere opravdu patri do fraktalni mnoziny (ktere generuji konvergentni posloupnost).
        /// </summary>
        public Color BARVA_MNOZINY = Color.White;
        /// <summary>
        /// Ovlada bitmapu vuci pixture boxu
        /// </summary>
        public BitmapaProPictureBox OvladacBitmapy;
        private int silaLupy = 2;
        /// <summary>
        /// Kolikrat se bude zvetsovat pri zmestseni (to same pro zmenseni)
        /// </summary>
        /// <value>minimalni hodnota je 2</value>
        public int SilaLupy
        {
            get { return silaLupy; }
            set
            {
                if (silaLupy < 2)
                    silaLupy = 2;
                else
                    silaLupy = value;
            }
        }
        /// <summary>
        /// cast komplexni roviny, nad kterou se bude generovat fraktal
        /// </summary>
        public VyrezKomplexniRoviny Rovina;
        /// <summary>
        /// Pole barevnych schemat z nichz lze vybirat
        /// </summary>
        private BarevneSchema[] poleBarevnychSchemat;
        private BarevneSchema aktualniBarevneSchema;
        /// <summary>
        /// Aktualne vybrane barevne schema
        /// </summary>
        public BarevneSchema AktualniBarevneSchema
        {
            get { return aktualniBarevneSchema; }
        }

        /// <summary>
        /// Cim vetsi, tim detajlneji se vykrasli fraktal, ale tim dele casu to zabere
        /// </summary>
        /// <remarks>Urcuje hranici - pocet clenu posloupnosti - podkud posloupnost dosahne teto hranice aniz by prekrocila normu = 2,
        /// tak se povazuje za konvergentni</remarks>
        public int PrahKonvergence = 200;
        /// <summary>
        /// Pole fraktalu z nichz lze vybirat
        /// </summary>
        private Fraktal[] poleFraktalu;
        private Fraktal aktualniFraktal;
        /// <summary>
        /// aktuane vybrany fraktal, jenz se bude vykraslovat
        /// </summary>
        public Fraktal AktualniFraktal
        {
            get { return aktualniFraktal; }
        }
        /// <summary>
        /// Inicializuje vsechny ridici struktuy potrebne pro chod mozku.
        /// </summary>
        /// <param name="kanvas">picture box nad kterym bude vytvorena bitmapa a vykreslovan fraktal.</param>
        public Mozek(PictureBox kanvas)
        {
           OvladacBitmapy = new BitmapaProPictureBox(kanvas);
           Rovina = new VyrezKomplexniRoviny(OvladacBitmapy.Bmp, kanvas);
           inicializujBarevnaSchemata();
           inicializujFraktaly();
        }

        /// <summary>
        /// Urci barvu a priradi ji jednomu pixelu v bitmape
        /// </summary>
        /// <param name="x">x souradnice daneho pixelu (musi spadat do 0..bitmapa.length-1 inkluzivne)</param>
        /// <param name="y">y souradnice daneho pixelu (musi spadat do 0..bitmapa.length-1 inkluzivne)</param>
        /// <param name="fraktal">fraktal, ktery ma byt vykreslen</param>
        /// <param name="rovina">komplexni rovina nad bitmapu ve ktere bude pixel vykreslen (obsahuje odkaz na onu bitmapu)</param>
        /// <param name="prahKonvergence">cim vyssi tim lespi matematicka presnost vykresleni fraktalu</param>
        /// <param name="barvicky">barevne schema pouzite pro obarveni fraktalu</param>
        private void vykresliPixelFraktalu(int x, int y, Fraktal fraktal, VyrezKomplexniRoviny rovina, int prahKonvergence, BarevneSchema barvicky)
        {
            bool konverguje;
            int i = fraktal.TestujKonvergenci(rovina.PriradKomplexniCislo(x, y), prahKonvergence, out konverguje);
            if (konverguje)
            {
                rovina.Bmp.SetPixel(x, y, BARVA_MNOZINY); //patri do mnoziny
            }
            else
            {
                rovina.Bmp.SetPixel(x, y, barvicky.UrciBarvu(i));
            }
        }
        /// <summary>
        /// Vykresli aktualni fraktal pres celou bitmapu a pouzije aktualni barevne schema. Hloubka detailu pritom zavisi na aktualnim Prahu Knovergence.
        /// </summary>
        public void VykresliFraktal()
        {
            vykresliFraktal(OvladacBitmapy.Kanvas, aktualniFraktal, Rovina, PrahKonvergence, aktualniBarevneSchema, OvladacBitmapy);
        }
        /// <summary>
        /// Vnitrni fuknce pro <c>public void VykresliFraktal()</c>
        /// </summary>
        /// <param name="kanvas">picture box nad kterym je bitmapa</param>
        /// <param name="fraktal">aktualne zvoleny fraktal</param>
        /// <param name="rovina">vyrez komplexni roviny aktualne prislusejici bitmape</param>
        /// <param name="prahKonvergence">hranice, ktera urcuje detail fraktalu</param>
        /// <param name="barvicky">aktualne zvolene barevne schema</param>
        /// <param name="ovladacBitmapy">pro obnovni bitmapy pred zacatkem vykraslovani</param>
        private void vykresliFraktal(PictureBox kanvas, Fraktal fraktal, VyrezKomplexniRoviny rovina, int prahKonvergence, BarevneSchema barvicky, BitmapaProPictureBox ovladacBitmapy)
        {
            rovina.Bmp = ovladacBitmapy.ObnovBitmapu();

            //pixely v bitmape jsou cislovane 0..(Bmp.Width-1)
            int x_levy, x_pravy, y_horni, y_spodni;
            int vyska, sirka;
            if ((rovina.Bmp.Width % 2) == 1)
            {
                x_pravy = rovina.Bmp.Width / 2;
                x_levy = x_pravy;
                sirka = 1;
            }
            else
            {
                x_pravy = rovina.Bmp.Width / 2;
                x_levy = x_pravy - 1;
                sirka = 2;
            }
            if ((rovina.Bmp.Height % 2) == 1)
            {
                y_horni = rovina.Bmp.Height / 2;
                y_spodni = y_horni;
                vyska = 1;
            }
            else
            {
                y_spodni = rovina.Bmp.Height / 2;
                y_horni = y_spodni - 1;
                vyska = 2;
            }

            if (sirka == 1)
            {
                if (vyska == 1)
                {
                    vykresliPixelFraktalu(x_pravy, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                }
                else //vyska 2
                {
                    vykresliPixelFraktalu(x_pravy, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(x_pravy, y_spodni, fraktal, rovina, prahKonvergence, barvicky);
                }
            }
            else //sirka 2
            {
                if (vyska == 1)
                {
                    vykresliPixelFraktalu(x_pravy, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(x_levy, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                }
                else //vyska 2
                {
                    vykresliPixelFraktalu(x_pravy, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(x_pravy, y_spodni, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(x_levy, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(x_levy, y_spodni, fraktal, rovina, prahKonvergence, barvicky);
                }
            }
            x_levy--; x_pravy++; y_horni--; y_spodni++;
            while ((x_levy >= 0) && (y_horni >= 0))
            {
                for (int i = y_horni; i <= y_spodni; i++)
                {
                    vykresliPixelFraktalu(x_levy, i, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(x_pravy, i, fraktal, rovina, prahKonvergence, barvicky);
                }
                for (int i = x_levy + 1; i <= x_pravy - 1; i++)
                {
                    vykresliPixelFraktalu(i, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                    vykresliPixelFraktalu(i, y_spodni, fraktal, rovina, prahKonvergence, barvicky);
                }
                x_levy--; x_pravy++; y_horni--; y_spodni++;
                kanvas.Refresh();
            }
            if (x_levy < 0)
            {
                while (y_horni >= 0)
                {
                    for (int i = 0; i < rovina.Bmp.Width; i++)
                    {
                        vykresliPixelFraktalu(i, y_horni, fraktal, rovina, prahKonvergence, barvicky);
                        vykresliPixelFraktalu(i, y_spodni, fraktal, rovina, prahKonvergence, barvicky);
                    }
                    y_horni--; y_spodni++;
                    kanvas.Refresh();
                }
            }
            else //y_spodni < 0
            {
                while (x_levy >= 0)
                {
                    for (int i = 0; i < rovina.Bmp.Height; i++)
                    {
                        vykresliPixelFraktalu(x_levy, i, fraktal, rovina, prahKonvergence, barvicky);
                        vykresliPixelFraktalu(x_pravy, i, fraktal, rovina, prahKonvergence, barvicky);
                    }
                    x_levy--; x_pravy++;
                    kanvas.Refresh();
                }
            }
        }
        /// <summary>
        /// Nastavi dane Barevne schema jako aktualni, je-li zvoleno aktualniSchema, neudela nic.
        /// </summary>
        /// <remarks>
        /// Vsechna barevna schemata museji byt naimplementovana (jako syni tridy BarevneSchema).
        /// Pokud ovsem nebudou, program nespadne a pouzije misto nich schema Duhy.
        /// </remarks>
        public void ZmenBarevneSchemaNa(BarevnaSchemata schema)
        {
            try
            {
                aktualniBarevneSchema = poleBarevnychSchemat[(int)schema];
            }
            catch
            {
                aktualniBarevneSchema = poleBarevnychSchemat[(int)BarevnaSchemata.Duha];
            }
        }
        /// <summary>
        /// Vytvori pole vsech naimplementovanych barevnych schemat a zvoli duhu jako aktualni schema.
        /// </summary>
        private void inicializujBarevnaSchemata()
        {
            poleBarevnychSchemat = new BarevneSchema[2];
            poleBarevnychSchemat[(int)BarevnaSchemata.Duha] = new Duha();
            poleBarevnychSchemat[(int)BarevnaSchemata.Vlastni] = new VlastniSchema();
            aktualniBarevneSchema = poleBarevnychSchemat[(int)BarevnaSchemata.Duha];
        }
        /// <summary>
        /// Vytvori pole vsech naimplementovanych fraktalu a zvoli mandelbrota jako aktualni fraktal.
        /// </summary>
        private void inicializujFraktaly()
        {
            poleFraktalu = new Fraktal[2];
            poleFraktalu[(int)Fraktaly.Mandelbrot] = new Mandelbrot();
            poleFraktalu[(int)Fraktaly.JuiliaJednoduchaKvadraticka] = new Julia_quadratic();
            aktualniFraktal = poleFraktalu[(int)Fraktaly.Mandelbrot];
        }
        /// <summary>
        /// Nastavi dany fraktal jako aktualni, je-li zvolen aktualniFraktal, neudela nic.
        /// </summary>
        /// <remarks>
        /// Vsechny fraktaly vyjmenovane v enum museji byt naimplementovane (jako syni tridy Fraktal).
        /// Pokud ovsem nebudou, program nespadne a pouzije misto nich Mandelbrota.
        /// </remarks>
        /// <param name="fraktal"></param>
        public void ZmenFraktalNa(Fraktaly fraktal)
        {
            try
            {
                aktualniFraktal = poleFraktalu[(int)fraktal];
            }
            catch
            {
                aktualniFraktal = poleFraktalu[(int)Fraktaly.Mandelbrot];
            }
        }
    }
}
























    /*


    public class Mozekstary
    {
        //pro dany pictureBox dynamicky vytvari bitmapu spravne velikosti
        //umoznuje kresbu fraktalu do komplexni roviny v bitmape

        //========rozliseni
        //public int RozliseniX;
        //public int RozliseniY;
        public bool RucneNastaveneRozliseni = false;

        //public int SilaLupy = 2; //int.MaxValue = 2 147 483 647
        //public double Zvetseni = 1;


        public StylVykraslovani StylVykresleni = StylVykraslovani.spiralovite;
        public TypFraktalu Typ = TypFraktalu.mandelbrot;

        //==========barvy
        public StylBarveni StylBarveni = StylBarveni.duha;
        //public int RychlostBarvy = 100;
        public bool PredchazelaDuha = true;

        //public int RychlostBarvy_Duha = 100;
        //public int PosunBarvy = 0;

        //public Color[] VlastniBarvy = new Color[8]{Color.Blue, Color.Green, Color.Red, Color.Yellow, Color.Pink, Color.Purple, Color.White, Color.Orange};
        //public int RychlostBarvy_vlastni = 100;

        //===========komplexni rovina
        //public PictureBox ObrazBox;
        //public Bitmap Bmp; //pixel 0,0 je levy horni roh
        //public Bitmap Bmp_old;
        //public double StredKomplexniRoviny_x, StredKomplexniRoviny_y, ReRozpeti, SirkaPixelu;
        
        //public int PRAHKONVERGENCE = 200;
        //public Komplex Seminko = new Komplex(0.279, 0.0); //pro Julia

        
        //===============================================================================metody

       // public Mozekstary(PictureBox obrazBox)
      //  {
          //  ObrazBox = obrazBox;
         //   RozliseniX = ObrazBox.Width;
          //  RozliseniY = ObrazBox.Height;
         //   Bmp = new Bitmap(RozliseniX, RozliseniY); //aby nedoslo k problemu s ukladanim (kdyby bitmapa byla null)

            //inicializace komplexni roviny
           // StredKomplexniRoviny_x = -1.0; //uprostred bitmapy
            //StredKomplexniRoviny_y = 0.0;
            //ReRozpeti = 3.0;
       // }

        //hotovson
        public void Kalibruj_KomplexniRovinu(int x, int y, bool priblizit)
        {
            //zmeni stred a realne rozpeti (priblizenost) po zoomu (double clicku)
            double oKolikDoleva = (Bmp.Width / 2 - x) * SirkaPixelu;
            double oKolikNahoru = (Bmp.Height / 2 - y) * SirkaPixelu;

            //novy stred
            StredKomplexniRoviny_x -= oKolikDoleva;
            StredKomplexniRoviny_y += oKolikNahoru;
            if (priblizit) //zvetseni
            {
                ReRozpeti /= SilaLupy;
            }
            else //zmenseni
            {
                ReRozpeti *= SilaLupy;
            }
        }
        //hotovson
        public void Kalibruj_KomplexniRovinu(int xNew, int xOld, int yNew, int yOld)
        {
            //zmeni stred po posunuti (drag)
            double oKolikDoleva = (xNew - xOld) * SirkaPixelu;
            double oKolikNahoru = (yNew - yOld) * SirkaPixelu;

            StredKomplexniRoviny_x -= oKolikDoleva;
            StredKomplexniRoviny_y += oKolikNahoru;
        }
        
        //hotovson
        public Komplex PriradKomplexniCislo(int x, int y)
        {
            //prevede pixel na odpovidajici komplexni cislo

            //ma-li bitmapa sudou vysku nebo sirku, na privni(nulou pojmenovany) pixel se zapomene a jako stred(komplexni roviny) se povazuje stred zbyleho licheho poctu
            //ma-li bitmapa lichou sirku, pak prostredni pixel ma souradnici x = Bmp.Width / 2;
            int prostredniPixel_x = Bmp.Width / 2;
            int prostredniPixel_y = Bmp.Height / 2;

            SirkaPixelu = ReRozpeti / Bmp.Width;

            return new Komplex(StredKomplexniRoviny_x + (x - prostredniPixel_x) * SirkaPixelu, StredKomplexniRoviny_y - (y - prostredniPixel_y) * SirkaPixelu);
        }
        //hotovson
        public void VykresliFraktal()
        {
            if (RucneNastaveneRozliseni)
            {
                ObnovBitmapu(RozliseniX, RozliseniY);
                RucneNastaveneRozliseni = false;
            }
            else
            {
                ObnovBitmapu();
            }

            SirkaPixelu = ReRozpeti / Bmp.Width;
            double ImRozpeti = Bmp.Height * SirkaPixelu;
            double ReZacatek = StredKomplexniRoviny_x - (ReRozpeti / 2);
            double ImZacatek = StredKomplexniRoviny_y - (ImRozpeti / 2);

            Komplex aktualniKomplexCislo = new Komplex(ReZacatek, ImZacatek);



            //pixely v bitmape jsou cislovane 0..(Bmp.Width-1)

            if (StylVykresleni == StylVykraslovani.spiralovite)
            {
                int x_levy, x_pravy, y_horni, y_spodni;
                int vyska, sirka;
                if ((Bmp.Width % 2) == 1)
                {
                    x_pravy = Bmp.Width / 2;
                    x_levy = x_pravy;
                    sirka = 1;
                }
                else
                {
                    x_pravy = Bmp.Width / 2;
                    x_levy = x_pravy - 1;
                    sirka = 2;
                }
                if ((Bmp.Height % 2) == 1)
                {
                    y_horni = Bmp.Height / 2;
                    y_spodni = y_horni;
                    vyska = 1;
                }
                else
                {
                    y_spodni = Bmp.Height / 2;
                    y_horni = y_spodni - 1;
                    vyska = 2;
                }

                if (sirka == 1)
                {
                    if (vyska == 1)
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, y_horni), x_pravy, y_horni);
                    }
                    else //vyska 2
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, y_horni), x_pravy, y_horni);
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, y_spodni), x_pravy, y_spodni);
                    }
                }
                else //sirka 2
                {
                    if (vyska == 1)
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, y_horni), x_pravy, y_horni);
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_levy, y_horni), x_levy, y_horni);
                    }
                    else //vyska 2
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, y_horni), x_pravy, y_horni);
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, y_spodni), x_pravy, y_spodni);
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_levy, y_horni), x_levy, y_horni);
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_levy, y_spodni), x_levy, y_spodni);
                    }
                }
                x_levy--; x_pravy++; y_horni--; y_spodni++;
                while ((x_levy >= 0)&&(y_horni >= 0))
                {
                    for (int i = y_horni; i <= y_spodni; i++)
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_levy, i), x_levy, i);
                        Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, i), x_pravy, i);
                    }
                    for (int i = x_levy + 1; i <= x_pravy - 1; i++)
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(i, y_horni), i, y_horni);
                        Generuj_BodFraktalu(PriradKomplexniCislo(i, y_spodni), i, y_spodni);
                    }
                    x_levy--; x_pravy++; y_horni--; y_spodni++;
                    ObrazBox.Refresh();
                }
                if (x_levy < 0)
                {
                    while (y_horni >= 0)
                    {
                        for (int i = 0; i < Bmp.Width; i++)
                        {
                            Generuj_BodFraktalu(PriradKomplexniCislo(i, y_horni), i, y_horni);
                            Generuj_BodFraktalu(PriradKomplexniCislo(i, y_spodni), i, y_spodni);
                        }
                        y_horni--; y_spodni++;
                        ObrazBox.Refresh();
                    }
                }
                else //y_spodni < 0
                {
                    while (x_levy >= 0)
                    {
                        for (int i = 0; i < Bmp.Height; i++)
                        {
                            Generuj_BodFraktalu(PriradKomplexniCislo(x_levy, i), x_levy, i);
                            Generuj_BodFraktalu(PriradKomplexniCislo(x_pravy, i), x_pravy, i);
                        }
                        x_levy--; x_pravy++;
                        ObrazBox.Refresh();
                    }
                }
            }
            else if (StylVykresleni == StylVykraslovani.poSloupcich)
            {
                for (int Xcount = 0; Xcount < Bmp.Width; Xcount++) //pres vsechny sloupce
                {
                    for (int Ycount = 0; Ycount < Bmp.Height; Ycount++) //shora dolu
                    {
                        Generuj_BodFraktalu(PriradKomplexniCislo(Xcount, Ycount), Xcount, Ycount);
                    }
                    ObrazBox.Refresh(); //po kazdem sloupecku
                }
            }
        }

        public void Generuj_BodFraktalu(Komplex komplexCislo, int souradnice_x, int souradnice_y)
        {
            if (Typ == TypFraktalu.julia)
            {
                GenerujJulia(komplexCislo, souradnice_x, souradnice_y, Seminko);
            }
            else //mandelbrot
            {
                GenerujMandelbrot(komplexCislo, souradnice_x, souradnice_y);
            }
        }

        //hotovson
        public void GenerujJuliaNova(Komplex komplexCislo, int souradnice_x, int souradnice_y, Komplex seminko)
        {
            //doporucena seminka jsou:

            //0.279 + 0.0i
            //0.432 + 0.432i
            //necht G = 1.61803398875 (zlaty rez)
            //1-G
            //G-2 + (G-1)i
            //-0.8 + 0.156i

            Komplex Zn = komplexCislo; Komplex a = new Komplex(0.6, 0.0);
            for (int i = 1; i < PRAHKONVERGENCE; i++)
            {
                Zn = (Zn * Zn * Zn) - (a * Zn) + seminko;
                if (Zn.Norma >= 2)
                {
                    //pak Zn v dalsich iteracich musi divergovat
                    //dle rychlosti divergence zvolme barvu
                    Bmp.SetPixel(souradnice_x, souradnice_y, UrciBarvu(i));
                    break;
                }
            }
            if (Zn.Norma < 2)
            {
                Bmp.SetPixel(souradnice_x, souradnice_y, Color.Black);
            }
        }

        //hotovson
        public void GenerujJulia(Komplex komplexCislo, int souradnice_x, int souradnice_y, Komplex seminko)
        {
            //doporucena seminka jsou:

            //0.279 + 0.0i
            //0.432 + 0.432i
            //necht G = 1.61803398875 (zlaty rez)
            //1-G
            //G-2 + (G-1)i
            //-0.8 + 0.156i

            Komplex Zn = komplexCislo;
            for (int i = 1; i < PRAHKONVERGENCE; i++)
            {
                Zn = (Zn * Zn) + seminko;
                if (Zn.Norma >= 2)
                {
                    //pak Zn v dalsich iteracich musi divergovat
                    //dle rychlosti divergence zvolme barvu
                    Bmp.SetPixel(souradnice_x, souradnice_y, UrciBarvu(i));
                    break;
                }
            }
            if (Zn.Norma < 2)
            {
                Bmp.SetPixel(souradnice_x, souradnice_y, Color.Black);
            }
        }

        //hotovson
        public void GenerujMandelbrot(Komplex komplexCislo, int souradnice_x, int souradnice_y)
        {
            Komplex Zn = komplexCislo;

            for (int i = 1; i < PRAHKONVERGENCE; i++)
            {
                Zn = (Zn * Zn) + komplexCislo;
                if (Zn.Norma >= 2)
                {
                    //pak Zn v dalsich iteracich musi divergovat
                    //dle rychlosti divergence zvolme barvu
                    Bmp.SetPixel(souradnice_x, souradnice_y, UrciBarvu(i));
                    break;
                }
            }
            if (Zn.Norma < 2)
            {
                Bmp.SetPixel(souradnice_x, souradnice_y, Color.Black);
            }
        }

        //hotovson
        public Color UrciBarvu(int i)
        {
            if (StylBarveni == StylBarveni.duha)
            {
                return UrciBarvuDuhy(i);
            }
            else //vlastni
            {
                return UrciBarvuVlastni(i);
            }
        }

        //============vlastni barvy
        public void PrejdiDoRezimuVlastnichBarev(Color[] barvy)
        {
            ObnovVlastnostiBarev();
            VlastniBarvy = barvy;
            PrepocitejRychlostVlastnichBarev();
            StylBarveni = StylBarveni.vlastni;
        }

        public void PrepocitejRychlostVlastnichBarev()
        {
            int pocetVlastnichBarev = VlastniBarvy.Length;
            if (pocetVlastnichBarev > RychlostBarvy)
            {
                RychlostBarvy = pocetVlastnichBarev;
            }
            else
            {
                RychlostBarvy = RychlostBarvy - (RychlostBarvy % pocetVlastnichBarev);
            }//nyni RychlostBarvy_vlastni je nasobkem poctu barev
        }

        public void NactiRychlostBarvy(int rychlost)
        {
            if (StylBarveni == StylBarveni.duha)
            {
                RychlostBarvy = rychlost;
                PosunBarvy = PosunBarvy % RychlostBarvy;
                //PosunBarvy je mezi 0..RychlostBarvy-1 (vzdy)
            }
            else //vlastni
            {
                RychlostBarvy = rychlost;
                PrepocitejRychlostVlastnichBarev();
            }
        }

        //hotovson
        public void ObnovVlastnostiBarev()
        {
            if (PredchazelaDuha)
            {
                RychlostBarvy = 100;
                PosunBarvy = 0;
            }
        }

        //hotovson
        public Color UrciBarvuVlastni(int i)
        {
            //pred pouzitim musela byt volana funkce PrejdiDoRezimuVlastnichBarev
            int barevnychMezistupnu = RychlostBarvy / VlastniBarvy.Length; //vzdy alespon 1
            int uroven = ((i + PosunBarvy)% RychlostBarvy) + 1; //1..BarevnychUrovni
            int index = (uroven - 1) / barevnychMezistupnu; //0..VlastniBarvy.Length-1
            int offset = (uroven - 1) % barevnychMezistupnu;

            int dalsi_index;
            if (index == VlastniBarvy.Length - 1)
            {
                dalsi_index = 0;
            }
            else
            {
                dalsi_index = index + 1;
            }
            Color a = VlastniBarvy[index]; Color b = VlastniBarvy[dalsi_index];
            double cast = (double)offset / barevnychMezistupnu;
            int red = (int)(a.R + (b.R - a.R) * cast);
            int green = (int)(a.G + (b.G - a.G) * cast);
            int blue = (int)(a.B + (b.B - a.B) * cast);

            return Color.FromArgb(red, green, blue);
        }

        //hotovson
        public Color UrciBarvuDuhy(int i)
        {
            return HslToRgb(((i + PosunBarvy) % RychlostBarvy) / (double)RychlostBarvy, 1.0, 0.5); //hue v intevalu [0,1)
        }
       
        //hotovson
        public Color HslToRgb(double h, double sl, double l)
        {
            //duha
            //?? predelat viz. Form1
            //http://www.geekymonkey.com/Programming/CSharp/RGB2HSL_HSL2RGB.htm


            //vsechny hodnoty (h,s,l) se pohybuji mezi 0.0 ... 1.0
            double v;
            double r, g, b;

            r = l;   //zacneme se sedou
            g = l;
            b = l;

            v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);

            if (v > 0)
            {
                double m;
                double sv;
                int sextant;
                double fract, vsf, mid1, mid2;

                m = l + l - v;
                sv = (v - m) / v;
                h *= 6.0;
                sextant = (int)h;
                fract = h - sextant;
                vsf = v * sv * fract;
                mid1 = m + vsf;
                mid2 = v - vsf;

                switch (sextant)
                {
                    case 0:
                        r = v;
                        g = mid1;
                        b = m;
                        break;
                    case 1:
                        r = mid2;
                        g = v;
                        b = m;
                        break;
                    case 2:
                        r = m;
                        g = v;
                        b = mid1;
                        break;
                    case 3:
                        r = m;
                        g = mid2;
                        b = v;
                        break;
                    case 4:
                        r = mid1;
                        g = m;
                        b = v;
                        break;
                    case 5:
                        r = v;
                        g = m;
                        b = mid2;
                        break;
                }
            }
            return Color.FromArgb((int)(r * 255.0f), (int)(g * 255.0f), (int)(b * 255.0f));
            //255.0f urcuje typ float konstanty pro compiler
        }
        //hotovson
        public void ObnovBitmapu()
        {
            //zahodi starou a vytvori novou bitmapu, odpovidajici velikosti (odpovida velikosti ObrazBoxu)
            if (Bmp_old != null)
            {
                Bmp_old.Dispose();
            }
            Bmp_old = Bmp;
            ObrazBox.BackgroundImage = Bmp_old;
            RozliseniX = ObrazBox.Width;
            RozliseniY = ObrazBox.Height;
            Bmp = new Bitmap(RozliseniX, RozliseniY);
            ObrazBox.SizeMode = PictureBoxSizeMode.Normal;
            ObrazBox.Image = Bmp;
        }
        //hotovson
        public void ObnovBitmapu(int rozliseniX, int rozliseniY)
        {
            //vytvori bitmapu dle rucne zadaneho rozliseni a zahodi starou, pritom zaruci aby se bitmapa naskalovala a byla zobrazena cela v picture boxu
            if (Bmp_old != null)
            {
                Bmp_old.Dispose();
            }
            Bmp_old = Bmp;
            ObrazBox.BackgroundImage = Bmp_old;
            RozliseniX = rozliseniX;
            RozliseniY = rozliseniY;
            Bmp = new Bitmap(RozliseniX, RozliseniY);
            ObrazBox.SizeMode = PictureBoxSizeMode.StretchImage;
            ObrazBox.Image = Bmp;
        }
    }
}


    */