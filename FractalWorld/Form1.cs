﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalWorld
{
    /// <summary>
    /// Hlavni okno ve kterem se odehrava cely program a po jeho zavreni skonci.
    /// </summary>
    public partial class Form1 : Form
    {
       
        /// <summary>
        /// pomocne promenne pro dragovani.
        /// </summary>
        private int pozicePoslednihoStisknutiMysi_x, pozicePoslednihoStisknutiMysi_y;
        /// <summary>
        /// Ovladac veskere funkcionality.
        /// </summary>
        private Mozek mo;

        public Form1()
        {
            UnmanagedCode.DisableProcessWindowsGhosting();
            InitializeComponent();
            mo = new Mozek(pictureBox1);
        }
        /// <summary>
        /// Vykresli Fraktal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_draw_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Drawing"; this.Refresh();
            mo.VykresliFraktal();
            toolStripStatusLabel1.Text = "Done";
        }
        /// <summary>
        /// Priblizi Fraktal. (dvojklik levym tlacitkem)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                toolStripStatusLabel1.Text = "Zooming in"; this.Refresh();
                mo.Rovina.Zoomuj(e.X, e.Y, true, mo.SilaLupy);
                mo.VykresliFraktal();
                toolStripStatusLabel1.Text = "Done";
            } //pravy double click nic
        }
        /// <summary>
        /// Oddali fraktal. (pravym kliknutim)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                toolStripStatusLabel1.Text = "Zooming out"; this.Refresh();
                mo.Rovina.Zoomuj(e.X, e.Y, false, mo.SilaLupy);
                mo.VykresliFraktal();
                toolStripStatusLabel1.Text = "Done";
            }
        }
        /// <summary>
        /// Zapamatuje si pozici mysi, pro pripad, ze by melo dojit k tahani. (libovolnym tlacitkem mysi)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            pozicePoslednihoStisknutiMysi_x = e.X;
            pozicePoslednihoStisknutiMysi_y = e.Y;
        }
        /// <summary>
        /// Posune fraktal, pokud bylo dostatecne zatahnuto.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            //pokud mys dragovala vice nez 3 pixely
            if (Math.Abs(pozicePoslednihoStisknutiMysi_x - e.X) > 3 || Math.Abs(pozicePoslednihoStisknutiMysi_y - e.Y) > 3)
            {
                //dojde k posunu fraktalu
                toolStripStatusLabel1.Text = "Moving"; this.Refresh();
                mo.Rovina.Posun(e.X, pozicePoslednihoStisknutiMysi_x, e.Y, pozicePoslednihoStisknutiMysi_y);
                mo.VykresliFraktal();
                toolStripStatusLabel1.Text = "Done";
            }
            //jinak se nic nestane
        }
        /// <summary>
        /// Umozni ulozit posledni vykresleny fraktal jako obrazek. Formaty png, jpeg, gif, bmp. Dojde-li k vyjimce, zmeni status na "Saving Failed"
        /// a neprovede nic.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //!! GDI+(Microsotfi graficke API) nepodporuje .ico encoder (pouze decoder) ..proto by bylo ukladani ikon slozitejsi a tudiz zadne nebude
            using (SaveFileDialog sd = new SaveFileDialog())
            {
                sd.Filter = "PNG Image|*.png|JPeg Image|*.jpg|Gif Image|*.gif|Bitmap Image|*.bmp";
                sd.Title = "Save Fractal as Image";
                toolStripStatusLabel1.Text = "Saving"; this.Refresh();
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        System.IO.FileStream fs = (System.IO.FileStream)sd.OpenFile(); //otevre se existujici soubor, nebo se vytvori novy
                        switch (sd.FilterIndex)
                        {
                            case 1:
                                mo.OvladacBitmapy.Bmp.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
                                break;
                            case 2:
                                mo.OvladacBitmapy.Bmp.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                                break;
                            case 3:
                                mo.OvladacBitmapy.Bmp.Save(fs, System.Drawing.Imaging.ImageFormat.Gif);
                                break;
                            case 4:
                                mo.OvladacBitmapy.Bmp.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                                break;
                        }
                        fs.Close(); //uvolni sys. resources (file deskripory atd.)
                        toolStripStatusLabel1.Text = "Image saved";
                    }
                    catch //nejake necekane vyjimky
                    {
                        toolStripStatusLabel1.Text = "Saving falied.";
                    }
                }
            }
        }
        /// <summary>
        /// Dovoli zmenit rozliseni. (pouze na jedno prekresleni - pak se rozliseni zase nastavi automaticky dle pictureBoxu).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_rozliseni fr = new Form_rozliseni(mo.OvladacBitmapy.Rozliseni_X, mo.OvladacBitmapy.Rozliseni_Y))
            {
                if (fr.ShowDialog() == DialogResult.OK)
                {
                    mo.OvladacBitmapy.nastavRozliseni(fr.PrectiSirku(), fr.PrectiVysku());
                }
            }
        }
        /// <summary>
        /// Umozni nastavit silu zvetsovani/zmensovani.
        /// </summary>
        /// <remarks>
        /// Na integer hodnotu mezi 2 a int.Maxvalue.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void magnificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_pocitadlo fp = new Form_pocitadlo(mo.SilaLupy, 2, int.MaxValue, "Magnification", "Change magnification:"))
            {
                if (fp.ShowDialog() == DialogResult.OK)
                {
                    mo.SilaLupy = fp.PrectiHodnotu();
                }
            }
        }
        /// <summary>
        /// Umozni prohloubit detail prodlouzenim vypoctu (zmena PrahuKonvergence)
        /// </summary>
        /// <remarks>
        /// Nejmensi dosazitelny detajl je 20, nejvetsi int.Maxvalue</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void detailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_pocitadlo fp = new Form_pocitadlo(mo.PrahKonvergence, 20, int.MaxValue, "Detail", "Increase computation time:"))
            {
                if (fp.ShowDialog() == DialogResult.OK)
                {
                    mo.PrahKonvergence = fp.PrectiHodnotu();
                }
            }
        }
        /// <summary>
        /// Umozni presne zmenit pozici kde se ve fraktalu nachazime.
        /// </summary>
        /// <remarks>
        /// Pozice je udana stredem - komplexnim cislem uprostred bitmapy - a realnym rozpetim - jaka cast realne primky je viditelna.
        /// Imaginarni rozpeti se dopocita samo.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void coordinatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_souradnice fs = new Form_souradnice(mo.Rovina.Stred, mo.Rovina.ReRozpeti, mo.Rovina.Zvetseni))
            {
                if (fs.ShowDialog() == DialogResult.OK)
                {
                    mo.Rovina.ZmenSouradnice(fs.PrectiStred(), fs.PrectiRealneRozpeti());
                }
            }
        }
        /// <summary>
        /// Nastavi barevne schema na Duhu.
        /// </summary>
        /// <remarks>Zakaze volbu duhy, jelikoz uz je zvolena</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rainbowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rainbowToolStripMenuItem.Enabled = false;
            mo.ZmenBarevneSchemaNa(BarevnaSchemata.Duha);
        }
        /// <summary>
        /// Nastavi barevne schema na vlastni schema (rucne vybrane barvy) a da moznost ihned barvy nastavit.
        /// </summary>
        /// <remarks>Povoli volbu duhy.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void customToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rainbowToolStripMenuItem.Enabled = true;
            mo.ZmenBarevneSchemaNa(BarevnaSchemata.Vlastni);

            using (Form_vlastniBarvy fb = new Form_vlastniBarvy(((VlastniSchema)mo.AktualniBarevneSchema).Barvy))
            {
                if (fb.ShowDialog() == DialogResult.OK)
                {
                    ((VlastniSchema)mo.AktualniBarevneSchema).Barvy = fb.PrectiBarvy();
                }
            }
        }
        /// <summary>
        /// Umoznuje zmenit rychlost barvy - pocet ruznych barev (barevnych stupne) v jednom spektru - zjemnuje barevny prechod.
        /// </summary>
        /// <remarks>Minimalni rychlost je 10, maximalni int.Maxvalue</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void speedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_pocitadlo fp = new Form_pocitadlo(mo.AktualniBarevneSchema.Rychlost, 10, int.MaxValue, "Color speed", "Change number of color levels:"))
            {
                if (fp.ShowDialog() == DialogResult.OK)
                {
                    mo.AktualniBarevneSchema.Rychlost = fp.PrectiHodnotu();
                }
            }
        }
        /// <summary>
        /// Umozni posunout barevne spektrum o nekolik stupnu (vuci "tovarnimu" nastaveni - nikoliv poslednimu nastaveni).
        /// </summary>
        /// <remarks>Minimalni posun je 0 a maximalni je pocet barenych stupne v jedno spektru (rychlost barvy). Obe dve maji stejny efekt.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shiftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_pocitadlo fp = new Form_pocitadlo(mo.AktualniBarevneSchema.Posun, 0, mo.AktualniBarevneSchema.Rychlost, "Color shift", "Shift the color spectrum by:"))
            {
                if (fp.ShowDialog() == DialogResult.OK)
                {
                    mo.AktualniBarevneSchema.Posun = fp.PrectiHodnotu();
                }
            }
        }
        /// <summary>
        /// Umozni zmenit barvu fraktalni mnoziny (white territory).
        /// </summary>
        private void innerColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ColorDialog cd = new ColorDialog())
            {
                if (cd.ShowDialog() == DialogResult.OK)
                {
                    mo.BARVA_MNOZINY = cd.Color;
                }
            }
        }
        /// <summary>
        /// Zvoli fraktal Mandelbrot.
        /// </summary>
        /// <remarks>Zakaze volbu mandelbrota, jelikoz uz je zvoleny.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mandelbrotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mandelbrotToolStripMenuItem.Enabled = false;
            mo.ZmenFraktalNa(Fraktaly.Mandelbrot);
        }
        /// <summary>
        /// Zvoli fraktal jednoducha kvadraticka Julia a ihned umozni zvolit seminko, ktere ovlivneje jeji tvar.
        /// </summary>
        /// <remarks>Povoli volbu Mandelbrota.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void juliaQuadraticToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mandelbrotToolStripMenuItem.Enabled = true;
            mo.ZmenFraktalNa(Fraktaly.JuiliaJednoduchaKvadraticka);
            using(Form_kvadratickaJulia fj = new Form_kvadratickaJulia(((Julia_quadratic)mo.AktualniFraktal).Seminko))
            {
                if (fj.ShowDialog() == DialogResult.OK)
                {
                    ((Julia_quadratic)mo.AktualniFraktal).Seminko = fj.PrectiSeminko(); 
                }
            }
        }
        /// <summary>
        /// Zobrazi navod na pouziti.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void usageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_navodNaPouziti fp = new Form_navodNaPouziti())
            {
                fp.ShowDialog();
            }
        }
        /// <summary>
        /// Zobrazi info o autorovi.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form_autor fa = new Form_autor())
            {
                fa.ShowDialog();
            }
        }
        




        /// <summary>
        /// Meni WndProc (zpusob zpracovani Windows messages) tak, aby dvojkliknuti na title bar
        /// nezpusobilo maximalizaci okna. Jelikoz se to spatne chova ve spojeni se Zoomem.
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x00A3) //dvojklik na title bar (tedy non-client area tohoto formu)
            {
                m.Result = IntPtr.Zero;
                return;
            }
            base.WndProc(ref m);
        }






        /*
        //=========================================
        //Zachytavani escape - predpokladam, ze toto je jedna z metod volanych metodou WndProc
        //http://stackoverflow.com/questions/18236067/can-you-add-an-esc-key-shortcut
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                // TODO: prerusovaci mechanizmus
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        //!!nastudovat
        //====================================================
        */
    }
}
